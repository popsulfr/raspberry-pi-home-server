Raspberry Pi Home Server

- https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4

Prepare SD card

```sh
sudo fdisk /dev/mmcblk0
> o
> n
> p
> ENTER
> +1G
> t
> c
> n
> p
> ENTER
> ENTER
> w

sudo mkfs.vfat /dev/mmcblk0p1
```

Encrypt the second partition

```sh
sudo cryptsetup --cipher xchacha12,aes-adiantum-plain64 --sector-size 4096 luksFormat /dev/mmcblk0p2
sudo cryptsetup --perf-no_read_workqueue --perf-no_write_workqueue --allow-discards --persistent open /dev/mmcblk0p2 picryptroot
# dd bs=512 count=4 if=/dev/random of=./files/crypto_keyfile.bin iflag=fullblock
sudo cryptsetup luksAddKey /dev/mmcblk0p2 ./files/crypto_keyfile.bin
```

Prepare Btrfs

```sh
sudo mkfs.btrfs -f /dev/mapper/picryptroot
mkdir rbpi4
sudo mount -o noatime,lazytime,compress-force=zstd:6,autodefrag,ssd_spread,commit=120,subvol=/ /dev/mapper/picryptroot rbpi4
sudo btrfs subvolume create rbpi4/@
sudo btrfs subvolume create rbpi4/@home
sudo umount rbpi4
sudo mount -o noatime,lazytime,compress-force=zstd:6,autodefrag,ssd_spread,commit=120,subvol=@ /dev/mapper/picryptroot rbpi4
sudo mkdir rbpi4/boot rbpi4/home
sudo mount -o noatime,lazytime,compress-force=zstd:6,autodefrag,ssd_spread,commit=120,subvol=@home /dev/mapper/picryptroot rbpi4/home
sudo mount /dev/mmcblk0p1 rbpi4/boot
```

Deploy Archlinux ARM image

```sh
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-aarch64-latest.tar.gz{,.md5,.sig}
md5sum -c ArchLinuxARM-rpi-aarch64-latest.tar.gz.md5
gpg --verify ArchLinuxARM-rpi-aarch64-latest.tar.gz.sig
sudo bsdtar -xpf ArchLinuxARM-rpi-aarch64-latest.tar.gz -C rbpi4
```

TODO
