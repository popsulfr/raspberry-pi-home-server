server {
        listen 80 default_server;
	listen [::]:80 default_server;
	server_name _;

	access_log off;

	root /srv/http/pihole;
	autoindex off;

	proxy_intercept_errors on;
	error_page 404 /pihole/index.php;

	index pihole/index.php index.php index.html index.htm;

	location / {
		expires max;
		try_files $uri $uri/ =404;
		more_set_headers 'X-Pi-hole: A black hole for Internet advertisements';
	}

	location ~ \.php$ {
		include fastcgi.conf;
		fastcgi_intercept_errors on;
		fastcgi_param SERVER_NAME $host;
		fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
	}
	
	location /admin {
		return 301 https://popsulfr.duckdns.org$request_uri;
	}
	
	location ~ /admin/\. {
		deny all;
	}

	location ~ /\.ht {
		deny all;
	}

	# . files
	location ~ /\.(?!well-known) {
		deny all;
	}

	# assets, media
	location ~* \.(?:css(\.map)?|js(\.map)?|jpe?g|png|gif|ico|cur|heic|webp|tiff?|mp3|m4a|aac|ogg|midi?|wav|mp4|mov|webm|mpe?g|avi|ogv|flv|wmv)$ {
		access_log off;
	}

	# svg, fonts
	location ~* \.(?:svgz?|ttf|ttc|otf|eot|woff2?)$ {
		more_set_headers 'Access-Control-Allow-Origin: *';
		access_log off;
	}
}

server {
	listen 80;
	listen [::]:80;
	server_name popsulfr.duckdns.org;
	# Redirect all HTTP requests to HTTPS with a 301 Moved Permanently response.
	return 301 https://popsulfr.duckdns.org$request_uri;
}

upstream doh-backend {
	server 127.0.0.1:8053;
}

upstream netdata {
	server unix:/var/run/netdata/netdata.sock;
	keepalive 64;
}

server {
        listen 443 ssl http2;
	listen [::]:443 ssl http2;
	server_name popsulfr.duckdns.org;

	include conf.d/ssl-popsulfr.duckdns.org.conf;

	access_log off;

	root /srv/http/pihole;
	autoindex off;

	proxy_intercept_errors on;
	error_page 404 /pihole/index.php;

	index pihole/index.php index.php index.html index.htm;

	location / {
		expires max;
		try_files $uri $uri/ =404;
		more_set_headers 'X-Pi-hole: A black hole for Internet advertisements';
	}

	location ~ \.php$ {
		include fastcgi.conf;
		fastcgi_intercept_errors on;
		fastcgi_param SERVER_NAME $host;
		fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
	}
	
	location /admin {
		root /srv/http/pihole;
		index index.php index.html index.htm;
		more_set_headers 'X-Pi-hole: The Pi-hole Web interface is working!';
		more_set_headers 'X-Frame-Options: DENY';
	}

	location /dns-query {
		proxy_read_timeout 86400;
		proxy_pass http://doh-backend/dns-query;
	}

	location = /netdata {
		return 301 /netdata/;
	}

	location ~ /netdata/(?<ndpath>.*) {
		proxy_redirect off;
		proxy_set_header Connection "keep-alive";
		proxy_store off;
		proxy_pass http://netdata/$ndpath$is_args$args;
	}
	
	location ~ /admin/\. {
		deny all;
	}

	location ~ /\.ht {
		deny all;
	}

	# . files
	location ~ /\.(?!well-known) {
		deny all;
	}

	# assets, media
	location ~* \.(?:css(\.map)?|js(\.map)?|jpe?g|png|gif|ico|cur|heic|webp|tiff?|mp3|m4a|aac|ogg|midi?|wav|mp4|mov|webm|mpe?g|avi|ogv|flv|wmv)$ {
		access_log off;
	}

	# svg, fonts
	location ~* \.(?:svgz?|ttf|ttc|otf|eot|woff2?)$ {
		more_set_headers 'Access-Control-Allow-Origin: *';
		access_log off;
	}
}
